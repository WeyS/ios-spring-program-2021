import UIKit

struct WeatherForecast{
    var temperature: Double
    var isRain: Bool
    var date : Date
}

enum WeatherError : Error{
    case error(String)
}

class WeatherDataProvider{
    static let delegate = WeatherDataProvider()
    
    private var saveURL : URL?
    
    func configure(weatherServiceUrl: URL){
        saveURL = weatherServiceUrl
    }
    
    func getWeatherThrows(date: Date) throws -> WeatherForecast{
        precondition(saveURL != nil, "URL")
        if (1...5).contains(Int.random(in: 1...10)) {
            let weather = WeatherForecast(temperature: 29, isRain: true, date: date)
            return weather
        }else{
            throw WeatherError.error("Try again please!")
        }
    }
    
    func getWeatherOptional(date: Date) -> WeatherForecast?{
        precondition(saveURL != nil, "URL")
        if (1...5).contains(Int.random(in: 1...10)) {
            let weather = WeatherForecast(temperature: 29, isRain: true, date: date)
            return weather
        }
        return nil
    }
    
    func getWeatherCallbackResult(date: Date, callback:(Result<WeatherForecast, WeatherError>) -> ()){
        precondition(saveURL != nil, "URL")
        if (1...5).contains(Int.random(in: 1...10)) {
            let weather = WeatherForecast(temperature: 29, isRain: true, date: date)
            callback(Result.success(weather))
        }else{
            callback(Result.failure(WeatherError.error("Try again!")))
        }
    }
    
    func getWeatherCallbackOptionals(date: Date, callback:(WeatherForecast?, WeatherError?) -> ()){
        precondition(saveURL != nil, "URL")
        if (1...5).contains(Int.random(in: 1...10)) {
            let weather = WeatherForecast(temperature: 29, isRain: true, date: date)
            callback(weather , nil)
        }else{
            callback(nil , WeatherError.error("Try againn!"))
        }
    }
}

let data = WeatherDataProvider.delegate
data.configure(weatherServiceUrl: (URL(string: "MyHome")!))

do{
    try data.getWeatherThrows(date: Date())
}catch{
    print("No")
}

let weatherThrows1 = try? data.getWeatherThrows(date: Date())
let weatherThrows2 = try! data.getWeatherThrows(date: Date())

let weatherOptional = try? data.getWeatherOptional(date: Date())

let weatherCallBackRes = try? data.getWeatherCallbackResult(date: Date(), callback: {res in print(res)})

let weatherCallBackOpt = try? data.getWeatherCallbackOptionals(date: Date(), callback: { (res,arg)  in
    if let res1 = res{
        print(res1)
    }else{
        print(arg!)
    }
})
