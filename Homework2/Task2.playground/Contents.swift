import UIKit

let allStudents : Set = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let presentOnMonday : Set = [1, 2, 5, 6, 7]
let presentOnTuesday : Set = [3, 6, 8, 10]
let presentOnWednesday : Set = [1, 3, 7, 9, 10]

let whoVisitsMondayAndWednesday = presentOnMonday.intersection(presentOnWednesday).subtracting(presentOnTuesday).sorted()
let whoVisitsThreeDays = presentOnMonday.intersection(presentOnTuesday).intersection(presentOnWednesday)
let whoMissedAllClasses1 = allStudents.symmetricDifference(presentOnMonday)
let whoMissedAllClasses2 = allStudents.symmetricDifference(presentOnTuesday)
let whoMissedAllClasses3 = allStudents.symmetricDifference(presentOnWednesday)
let whoMissedAllClasses = whoMissedAllClasses1.intersection(whoMissedAllClasses2).intersection(whoMissedAllClasses3).sorted()
let whoVisitsTwoDays1 = presentOnMonday.intersection(presentOnTuesday)
let whoVisitsTwoDays2 = presentOnMonday.intersection(presentOnWednesday)
let whoVisitsTwoDays3 = presentOnTuesday.intersection(presentOnWednesday)
let whoVisitsTwoDays = whoVisitsTwoDays1.union(whoVisitsTwoDays2).union(whoVisitsTwoDays3).sorted()

print("Three days: \(whoVisitsThreeDays)")
print("Two days: \(whoVisitsTwoDays)")
print("Monday and Wednesday but not Tuesday: \(whoVisitsMondayAndWednesday)")
print("Missed all classes: \(whoMissedAllClasses)")


