import UIKit

let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print("for-in all:", terminator: " ")
for key in allStudents {
    if key < allStudents.count {
        print(key, terminator: ",")
    }else{
        print(key, terminator: "")
    }
}

print(".")
print("while all:", terminator: " ")
var index = 0
while allStudents.count > index{
    if index < allStudents.count - 1 {
        print(allStudents[index], terminator: ",")
    }else{
        print(allStudents[index], terminator: "")
    }
    index += 1
}

print(".")
print("repeat-while all:", terminator: " ")
var index1 = 0
repeat{
    if index1 < allStudents.count - 1 {
        print(allStudents[index1], terminator: ",")
    }else{
        print(allStudents[index1], terminator: "")
    }
    index1 += 1
}while allStudents.count > index1
print(".")


