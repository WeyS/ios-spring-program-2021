import UIKit

let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print("for-in even:", terminator: " ")
for value in allStudents where value % 2 == 0 {
    if value < allStudents.count {
        print(value, terminator: ",")
    }else{
        print(value, terminator: "")
    }
}

print(".")
print("while even:", terminator: " ")
var index = 0
while allStudents.count > index{
    if index < allStudents.count - 1 && allStudents[index] % 2 == 0{
        print(allStudents[index], terminator: ",")
    }else if allStudents[index] % 2 == 0{
        print(allStudents[index], terminator: "")
    }
    index += 1
}

print(".")
print("repeat-while even:", terminator: " ")
var index1 = 0
repeat{
    if index1 < allStudents.count - 1 && allStudents[index1] % 2 == 0{
        print(allStudents[index1], terminator: ",")
    }else if allStudents[index1] % 2 == 0{
        print(allStudents[index1], terminator: "")
    }
    index1 += 1
}while allStudents.count > index1
print(".")

print("for-in odd:", terminator: " ")
for key in allStudents {
    if key < allStudents.count - 1 && key % 2 == 1 {
        print(key, terminator: ",")
    }else if key % 2 == 1{
        print(key, terminator: "")
    }
}

print(".")
print("while odd:", terminator: " ")
var index2 = 0
while allStudents.count > index2{
    if index2 < allStudents.count - 2 && allStudents[index2] % 2 == 1{
        print(allStudents[index2], terminator: ",")
    }else if allStudents[index2] % 2 == 1{
        print(allStudents[index2], terminator: "")
    }
    index2 += 1
}

print(".")
print("repeat-while odd:", terminator: " ")
var index3 = 0
repeat{
    if index3 < allStudents.count - 2 && allStudents[index3] % 2 == 1{
        print(allStudents[index3], terminator: ",")
    }else if allStudents[index3] % 2 == 1{
        print(allStudents[index3], terminator: "")
    }
    index3 += 1
}while allStudents.count > index3
print(".")
