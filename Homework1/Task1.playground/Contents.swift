import Darwin

let firstInt = 12
let secondInt = 42
let thirdInt = 59

let arithmeticMean = (firstInt + secondInt + thirdInt) / 3

let sum = firstInt * secondInt * thirdInt
let geometricMean = pow(Double(sum) , 1.0 / 3.0)

print("Arithmetic mean of numbers \(firstInt) , \(secondInt) , \(thirdInt) = \(arithmeticMean)")
print("Geometric mean of numbers \(firstInt) , \(secondInt) , \(thirdInt) = \(round(100.0 * geometricMean) / 100.0)")

let a : Double = 1
let b : Double = 4
let c : Double = 4

let D = pow(b, 2) - 4 * a * c
if (D > 0) {
    let root1 = ((-b) - sqrt(D)) / (2 * a)
    let root2 = ((-b) + sqrt(D)) / (2 * a)
    print("Quadratic equation \(a)*x^2 + \(b)*x + \(c) = 0 has solutions root1 = \(root1) and root2 = \(root2)")
}else if (D == 0){
    let root = (-b) / (2 * a)
    print("Quadratic equation \(a)*x^2 + \(b)*x + \(c) = 0 has solution root = \(root)")
}else{
    print("D < 0, roots don't exist")
}


