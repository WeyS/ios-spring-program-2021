//
//  Sandbox.swift
//  SmallTasks
//
//  Created by Oleksandr Sulyma on 17.07.2021.
//

import Foundation


struct BankUser {
    var name: String
    var money: Int

    mutating func divideToHalfMoney(to anotherBankUser: inout BankUser) {
        let sum = money + anotherBankUser.money
        money = sum / 2
        anotherBankUser.money = sum - money
    }
}


class BankManager {

    func executeAllYouNeed() {
        var a = BankUser(name: "First", money: 100)
        var b = BankUser(name: "Second", money: 50)
        a.divideToHalfMoney(to: &b) //OK
        
        // uncomment to see an error
        //a.divideToHalfMoney(to: &a)         //Errors:
        // (1) Inout arguments are not allowed to alias each other;
        // (2) Overlapping accesses to 'a', but modification requires exclusive access; consider copying to a local variable
    }

}


// uncomment to execute exercise with conflicting access
//let instanceOfCorruptedRunner = BankManager().executeAllYouNeed()
