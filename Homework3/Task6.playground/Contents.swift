enum MyOptional {
    case none
    case some(Int)
}

protocol Arithmetic {
    static func +(lhs: Self, rhs: Self) -> Self
}

extension MyOptional : Arithmetic {
    static func + (lhs: MyOptional, rhs: MyOptional) -> MyOptional {
        switch (lhs , rhs) {
        case (MyOptional.some(let l), MyOptional.some(let r)):
            return MyOptional.some(l + r)
        default:
            return .none
        }
    }
}


func sum (num1: MyOptional, num2: MyOptional) -> MyOptional {
    let res = num1 + num2
    return res
}

print(sum(num1: MyOptional.some(34), num2: MyOptional.some(10)))
