import UIKit

let arr = [1, 2, 12, 5, 7, 88]
let result1 = arr.filter { (num) -> Bool in
    num % 2 == 0
}
let result2 = arr.reduce(0,+)
print(result1)
print(result2)

let sArr =  ["1", "2", "12", "bla", "5", "7", "88"]
let result3 = sArr.compactMap { Int($0) }
print(result3)
