import UIKit

indirect enum LinkedList {
    case End
    case add(String, LinkedList)
    
    func append(value: String) -> LinkedList {
        return .add(value, self)
    }
    
    var description: String {
        return "(\(LinkedList.End.append(value: "wow").append(value: "car").append(value: "dinner").append(value: "man")))"
        }
    
}

print(LinkedList.End.description)


