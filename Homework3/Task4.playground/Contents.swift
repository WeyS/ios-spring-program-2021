import UIKit

enum ErrorCode : Error {
    case Error(Double)
}

func quadraticMean(a : Double , b : Double , c : Double) throws -> (Double , Double){
    let D = pow(b, 2) - 4 * a * c
    if D < 0{
        throw ErrorCode.Error(D)
    }else if D > 0{
        let root1 = ((-b) - sqrt(D)) / (2 * a)
        let root2 = ((-b) + sqrt(D)) / (2 * a)
        return (root1 , root2)
    }else{
        let root = (-b) / (2 * a)
        return (root , 0)
    }
}

let res = try quadraticMean(a: 5, b: 2, c: 4)
print(res)
