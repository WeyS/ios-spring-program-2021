import UIKit

struct Employee {
    var firstName: String
    var lastName: String
}

struct Product {
    var name: String
}

class Company {
    var list: [Employee]
    var name: String
    
    func getProduct() -> Product {
        return Product(name: name)
    }
    
     required init() {
        fatalError("init() has not been implemented")
    }
    
    init(list: [Employee], name: String) {
        self.name = name
        self.list = list
    }
    
    convenience init?(employee: Employee?, name: String) {
        if let emp = employee {
            self.init(list : [emp] , name : name)
        } else {
            return nil
        }
    }
}

class StateRegistry​ {
    static var registeredCompanies : [Company] = []
    
    func addCompany(_ company : Company){
        StateRegistry​.registeredCompanies.append(company)
    }
    
    func checkName(name : String) -> Bool {
        if StateRegistry​.registeredCompanies.contains(where: { (Company) -> Bool in
            Company.name == name
        }) {
            return true
        }else{
            return false
        }
    }
}

class FoodCompany : Company{
    var qualityCertificate : String
    
    init?(employee : (String?, String?), name: String, qualityCertificate: String) {
        if let emp1 = employee.0, let emp2 = employee.1 {
            self.qualityCertificate = qualityCertificate
            super.init(list: [Employee.init(firstName: emp1, lastName: emp2)], name: name)
        }else{
            return nil
        }
    }
    required init() {
        fatalError("init() has not been implemented")
    }
}

class Project{
    var contractor​ : Company
    var name : String

    init?(name: String, company: Company){
        if StateRegistry​.registeredCompanies.contains(where: { (Company) -> Bool in
            Company.name == name
        }){
            print("Invalid name")
            return nil
        }
        self.name = name
        self.contractor​ = company
    }
}


let company1 = Company(list: [Employee.init(firstName: "Max", lastName: "Serg")], name: "Comp1")
let company2 = Company(employee: Employee.init(firstName: "", lastName: ""), name: "Comp2")
let company3 = Company(list: [Employee.init(firstName: "Sergey", lastName: "Vano")], name: "pro3")
let foodCompany = FoodCompany(employee: ("Max", "Melnyk"), name: "foodComp", qualityCertificate: "Yes")


let register = StateRegistry​()
register.addCompany(company1)
if let reg = company2{
    register.addCompany(reg)
}else{
    print("nil")
}
register.checkName(name: "Comp2")
register.addCompany(company3)


let pro1 = Project(name: "pro1", company: company1)
let pro2 = Project(name: "pro2", company: company2 ?? Company())
let pro3 = Project(name: "pro3", company: company3)

