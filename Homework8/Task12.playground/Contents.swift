import UIKit

class ThreadSafeString{

    private let queue = DispatchQueue(label: "com.epam.race-condition")
    private var value = ""
    
    public func appendOval(_ string : String){
        queue.async(flags: .barrier){
            self.value += string
        }
    }
    
    public var getter : String{
        var res = ""
        queue.sync {
            res = self.value
        }
        return res
    }
}

var barrier = ThreadSafeString()

for _ in 0...1000 {
    DispatchQueue.global(qos: .userInitiated).async {
        barrier.appendOval("🟠")
    }
    
    DispatchQueue.global(qos: .userInteractive).async {
        barrier.appendOval("🟣")
    }
    
    DispatchQueue.global(qos: .utility).async {
        barrier.appendOval("🔴")
    }
    
    DispatchQueue.global(qos: .background).async {
        barrier.appendOval("")
    }
    
    DispatchQueue.global(qos: .default).async {
        print(barrier.getter)
    }
    
    DispatchQueue.global(qos: .background).async {
        print(barrier.getter)
    }
    
    DispatchQueue.global(qos: .userInteractive).async {
        print(barrier.getter)
    }
}
