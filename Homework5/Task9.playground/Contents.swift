import UIKit

// 1 Task
print("---------------1 solution---------------")

enum Transmission : CaseIterable {
    case variator
    case tiptronic
    case robot
    case manually
}

struct Car {
    var model : String
    var power : Int
    var transmission : Transmission
}

extension Car : CustomStringConvertible {
    var description: String {
        return """
        --- --- ---
        Model car: \(model)
        Car power: \(power)
        Car transmission: \(transmission)
        --- --- ---
        """
    }
}

func carsGroup(cars : [Car]) -> ([[Car]]){
    var sorCars = [[Car]]()
    for trans in Transmission.allCases{
        let filterTransmission = cars.filter {trans.hashValue == $0.transmission.hashValue}
        sorCars.append(filterTransmission)
    }
    return sorCars
}

var carArray = [
    Car(model: "BMW M5", power: 635, transmission: .robot),
    Car(model: "Opel Astra", power: 121, transmission: .manually),
    Car(model: "Volkswagen Transporter", power: 99, transmission: .manually),
    Car(model: "Mercedes-Benz S63", power: 830, transmission: .tiptronic),
    Car(model: "Audi A3", power: 268, transmission: .variator),
    Car(model: "Volkswagen Golf GTI", power: 515, transmission: .robot)
]

print(carsGroup(cars: carArray))


// 2 Task
print("---------------2 solution---------------")

extension Transmission : Hashable{}

extension Array{
    func group(by: (_ element: Element) -> AnyHashable) -> [[Element]] {
        var sorCars = [[Element]]()
        for trans in Transmission.allCases {
            for elem in self {
                if by(elem).hashValue == trans.hashValue{
                    sorCars.append([elem])
                }
            }
           }
        return sorCars
}
}

var carArray1 = [
    Car(model: "BMW M5", power: 635, transmission: .robot),
    Car(model: "Opel Astra", power: 121, transmission: .manually),
    Car(model: "Volkswagen Transporter", power: 99, transmission: .manually),
    Car(model: "Mercedes-Benz S63", power: 830, transmission: .tiptronic),
    Car(model: "Audi A3", power: 268, transmission: .variator),
    Car(model: "Volkswagen Golf GTI", power: 515, transmission: .robot)
]

print(carArray1.group {$0.transmission})
